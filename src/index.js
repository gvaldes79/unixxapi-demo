const express = require('express');
const app = express();
const morgan = require('morgan');

// Configuraciones
app.set ('port', process.env.PORT || 3000); //esta linea chequea si existe un puerto defindo y sino usa el 3000
app.set ('json spaces', 2);

//Inicio Midleware
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());


// Configurar cabeceras y cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

//Rutas
app.use('/api', require('./routes/routes')); //declaro el endpoint defaul en /api en el archivo route.js
app.use('/api/user', require('./routes/user')); //deaclro el endpoint /api/user en el archivo users.js
app.use('/api/registro', require('./routes/registro')); //declaro el endpoint /api/registro en el archico registro.js
app.use('/api/mesa', require('./routes/mesa')); //declaro el endpoint /api/mesa en el archico mesa.js
app.use('/api/materia', require('./routes/materia')); //declaro el endpoint /api/materia en el archico materia.js
//Inicio el Server
app.listen(app.get('port'), () =>{
    console.log(`Server on port ${app.get('port')}`);
});