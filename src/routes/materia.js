//rutas del endpoint materia (ya declare en index que el default para este endpoint es /api/materia)

const { Router} = require('express'); //requiero el objeto router de express
const router = Router(); // ejecuto el objeto Router() y lo almaceno en una constante Router
const mariaDBconnection = require ('../database'); //requiero el objeto database donde declare la conexion.

// Genero un end point que lista todas las materias
router.get('/', (req, res) => {
    mariaDBconnection.query ('SELECT Nombre FROM apigaston.p_materias;', (err, rows, fields) =>{
            console.log ('listo Mesas de examen de la base');
        
            if(!err){
                res.json(rows);
            } else {
                console.log(err);
            }
            
        });
        
    });

    // genero un end point que lista la materia que se corresponde con un id especifico
    router.get('/:id', (req, res) => {
       
            const { id } = req.params;
            console.log('devuelvo la materia para el id: '+id);
            mariaDBconnection.query ('SELECT * FROM apigaston.p_materias WHERE id = ?', [id], (err, rows, fields)=> {
                if(!err){
                    res.json(rows[0]);
                } else {
                    console.log(err);
                }
                
            });
        });

           // genero un end point que me devuelve los profesores que hay para una materia
    router.get('/:id/profesor', (req, res) => {
       
        const { id } = req.params;
        console.log('devuelvo la materia para el id: '+id);
        mariaDBconnection.query ('SELECT * FROM apigaston.p_materias WHERE id = ?', [id], (err, rows, fields)=> {
            if(!err){
                res.json(rows[0]);
            } else {
                console.log(err);
            }
            
        });
    });

module.exports = router; //Exporto el modulo para que sea accesible desde otros archivos

/** */