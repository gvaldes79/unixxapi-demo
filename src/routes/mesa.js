//rutas del endpoint mesa (ya declare en index que el default para este endpoint es /api/mesa)

const { Router} = require('express'); //requiero el objeto router de express
const router = Router(); // ejecuto el objeto Router() y lo almaceno en una constante Router
const mariaDBconnection = require ('../database'); //requiero el objeto database donde declare la conexion.

router.get('/', (req, res) => {
    mariaDBconnection.query ('SELECT apigaston.p_sedes.Sede as Sede, apigaston.p_tiposexam.tipo as Examen, apigaston.p_materias.Nombre as Materia, apigaston.d_mesa.MesaFecha as Fecha, apigaston.d_mesa.Mesahora as Hora, apigaston.d_usuario.Nombre as Nombre, apigaston.d_usuario.Apellido as Apellido FROM apigaston.d_mesa inner join apigaston.p_tiposexam on apigaston.d_mesa.idTipoExamen = apigaston.p_tiposexam.id inner join apigaston.p_materias on apigaston.d_mesa.idMateria = apigaston.p_materias.Id inner join apigaston.p_sedes on apigaston.d_mesa.idSede = apigaston.p_sedes.Id inner join apigaston.r_mesaprofesor on apigaston.d_mesa.id = apigaston.r_mesaprofesor.idMesa inner join apigaston.d_usuario on apigaston.r_mesaprofesor.idProfesor = apigaston.d_usuario.id', (err, rows, fields) =>{
            console.log ('listo Mesas de examen de la base');
        
            if(!err){
                res.json(rows);
            } else {
                console.log(err);
            }
            
        });
        
        //res.send('Usuarios');
    });

    //creo un end point para listar los tipos de mesa de examen
    router.get('/tipo', (req, res) => {
        mariaDBconnection.query ('SELECT tipo FROM apigaston.p_tiposexam;', (err, rows, fields) =>{
                console.log ('listo tipos de Mesas de examen de la base');
            
                if(!err){
                    res.json(rows);
                } else {
                    console.log(err);
                }
                
            });
            
        });
    
    
   //creo un end point para listar los tipos de mesa de examen
   router.get('/tipo', (req, res) => {
    mariaDBconnection.query ('SELECT tipo FROM apigaston.p_tiposexam;', (err, rows, fields) =>{
            console.log ('listo tipos de Mesas de examen de la base');
        
            if(!err){
                res.json(rows);
            } else {
                console.log(err);
            }
            
        });
        
    });



//un get a la ruta /api/mesa/profesor/id: trae los datos de las mesas para un usuario ingresando el ID
router.get ('/profesor/:id', (req, res) => {
    const { id } = req.params;
    console.log(id);
    mariaDBconnection.query ('SELECT apigaston.p_sedes.Sede as Sede, apigaston.p_tiposexam.tipo as Examen, apigaston.p_materias.Nombre as Materia, apigaston.d_mesa.MesaFecha as Fecha, apigaston.d_mesa.Mesahora as Hora, apigaston.d_usuario.Nombre as Nombre, apigaston.d_usuario.Apellido as Apellido FROM apigaston.d_mesa inner join apigaston.p_tiposexam on apigaston.d_mesa.idTipoExamen = apigaston.p_tiposexam.id inner join apigaston.p_materias on apigaston.d_mesa.idMateria = apigaston.p_materias.Id inner join apigaston.p_sedes on apigaston.d_mesa.idSede = apigaston.p_sedes.Id inner join apigaston.r_mesaprofesor on apigaston.d_mesa.id = apigaston.r_mesaprofesor.idMesa inner join apigaston.d_usuario on apigaston.r_mesaprofesor.idProfesor = apigaston.d_usuario.id WHERE usuario = ?', [id], (err, rows, fields)=> {
        if(!err){
            res.json(rows);
        } else {
            console.log(err);
        }
        
    });
});

router.post ('/crear', (req, res) =>{
    const {fecha, hora, materia, tipoexam, profesorNombre, profesorApellido, sede} = req.body;
    console.log(req.body);
	const query = `CALL crearMesa (?, ?, ?, ?, ?, ?, ?)`;
    mariaDBconnection.query (query, [fecha, hora, materia, tipoexam, profesorNombre, profesorApellido, sede], (err, rows, fields)=> {
        if (!err){
            res.json({Status: 'Mesa Creada'});
        } else {
            console.log(err); 
        }
    
});
});
module.exports = router; //Exporto el modulo para que sea accesible desde otros archivos

/** */