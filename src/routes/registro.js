//rutas del endpoint registro (ya declare en index que el default es /api/registro)

const { Router} = require('express'); //requiero el objeto router de express
const router = Router(); // ejecuto el objeto Router() y lo almaceno en una constante Router
const mariaDBconnection = require ('../database'); //requiero el objeto database donde declare la conexion.

//un get a la ruta /api/registro/profesor/:id trae los registros asociados a un profesor ingresando el usuario
router.get ('/profesor/:id', (req, res) => {
    const { id } = req.params;
    console.log(id);
   // mariaDBconnection.query ('select * from apigaston.d_registro inner join apigaston.d_mesa on apigaston.d_registro.idMesa = apigaston.d_mesa.Id inner join apigaston.r_mesaprofesor on apigaston.d_mesa.Id = apigaston.r_mesaprofesor.idMesa where apigaston.r_mesaprofesor.idProfesor = ?', [id], (err, rows, fields)=> {
    mariaDBconnection.query ('SELECT * FROM apigaston.todos_registros where apigaston.todos_registros.Profeusuario = ?', [id], (err, rows, fields)=> {
   if(!err){
            //res.json(rows[0]);
            res.json(rows);
        } else {
            console.log(err);
        }
        
    });
});

//un get a la ruta /api/registro/Alumno/:id trae los registros asociados a un Alumno ingresando el usuario
router.get ('/alumno/:id', (req, res) => {
    const { id } = req.params;
    console.log(id);
   // mariaDBconnection.query ('select * from apigaston.d_registro inner join apigaston.d_mesa on apigaston.d_registro.idMesa = apigaston.d_mesa.Id inner join apigaston.r_mesaprofesor on apigaston.d_mesa.Id = apigaston.r_mesaprofesor.idMesa where apigaston.r_mesaprofesor.idProfesor = ?', [id], (err, rows, fields)=> {
    mariaDBconnection.query ('SELECT * FROM apigaston.todos_registros where apigaston.todos_registros.AlumnoUsuario = ?', [id], (err, rows, fields)=> {
   if(!err){
            //res.json(rows[0]);
            res.json(rows);
        } else {
            console.log(err);
        }
        
    });
});



//un get a la ruta /api/registro/:id trae los datos de un registro ingresando el ID
router.get ('/:id', (req, res) => {
    const { id } = req.params;
    console.log(id);
    mariaDBconnection.query ('select * from apigaston.d_registro WHERE id = ?', [id], (err, rows, fields)=> {
        if(!err){
            res.json(rows[0]);
        } else {
            console.log(err);
        }
        
    });
});

module.exports = router; 
