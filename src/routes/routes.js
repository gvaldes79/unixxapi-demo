//rutas del endpoint default (ya declare en index que el default es /api)

const { Router} = require('express'); //requiero el objeto router de express
const router = Router(); // ejecuto el objeto Router() y lo almaceno en una constante Router

router.get('/', (req, res) => {
    res.json({"Title":"Hola!"});
});

router.get('/test', (req, res) =>
{
    const data = {
        "Nombre" : "Gaston",
        "Apellido" : "Valdes"
    };
    res.json(data);
    console.log (data);
});

module.exports = router; 