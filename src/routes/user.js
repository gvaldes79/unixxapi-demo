//rutas del endpoint user (ya declare en index que el default para este endpoint es /api/user)
const { Router} = require('express'); //requiero el objeto router de express
const router = Router(); // ejecuto el objeto Router() y lo almaceno en una constante Router
const mariaDBconnection = require ('../database'); //requiero el objeto database donde declare la conexion.

//un get a la ruta default de este archivo es /api/user y trae todos los usuarios.
router.get ('/', (req, res) => {
    mariaDBconnection.query ('SELECT * FROM apigaston.d_usuario', (err, rows, fields) =>{
        console.log ('listo usuarios de la base');
        if(!err){
            res.json(rows);
        } else {
            console.log(err);
        }
        
    });
    
    //res.send('Usuarios');
});

//un get a la ruta /api/user/id: trae los datos de un usuario ingresando el ID
router.get ('/:id', (req, res) => {
        const { id } = req.params;
        console.log(id);
        mariaDBconnection.query ('SELECT * FROM apigaston.d_usuario WHERE id = ?', [id], (err, rows, fields)=> {
            if(!err){
                res.json(rows[0]);
            } else {
                console.log(err);
            }
            
        });
});

//un post a la ruta /api/user permite agregar o actualizar usuarios
router.post ('/', (req, res)=>{
    const {id, idRol, usuario, Nombre, Apellido, email, Telefono, pass } = req.body; 
    const query = `
     CALL userAddOrEdit (?, ?, ?, ?, ?, ?, ?, ?);
    `;
    mariaDBconnection.query (query, [id, idRol, usuario, Nombre, Apellido, email, Telefono, pass], (err, rows, fields)=> {
if (!err){
    res.json({Status: 'Usuario Guardado'});
} else {
    console.log(err); 
}

    });
});

router.get ('/validate', (req, res)=>{
    res.send('validacion');
})



router.post ('/validate', (req, res) =>{
    const {usuario, pass} = req.body;
    console.log(req.body);
    if (usuario && pass) {
        //res.send ('Datos recividos');
       // mariaDBconnection.query ('select apigaston.p_roles.Descripcion from apigaston.d_usuario inner join apigaston.p_roles on apigaston.d_usuario.idRol = apigaston.p_roles.Id WHERE usuario = ? && pass = ?', [usuario, pass] , (err, rows, fields) => {
            mariaDBconnection.query ('select * from apigaston.d_usuario inner join apigaston.p_roles on apigaston.d_usuario.idRol = apigaston.p_roles.Id WHERE usuario = ? && pass = ?', [usuario, pass] , (err, rows, fields) => {
       
            if(!err){
                res.json(rows[0]);
                
                } else {
                    console.log(err);
                }
            });
        }
    else {
        res.send ('Faltan datos');
    }
});


// aca empiezo a jugar con el endpoint para validar los usuarios
router.get ('/validate/:usuario/:pass', (req, res) => {
    const { usuario, pass } = req.params;
    console.log(usuario);
    console.log(pass);
    if (usuario && pass) {
        console.log ('enviando datos');
//        mariaDBconnection.query ('SELECT * FROM apigaston.d_usuario WHERE usuario = ? && pass = ?', [usuario, pass] , (err, rows, fields) => {
        mariaDBconnection.query ('select apigaston.p_roles.Descripcion from apigaston.d_usuario inner join apigaston.p_roles on apigaston.d_usuario.idRol = apigaston.p_roles.Id WHERE usuario = ? && pass = ?', [usuario, pass] , (err, rows, fields) => {
        if(!err){
        res.json(rows[0]);
        } else {
            console.log(err);
        }
    });
    }
    else {
        console.log ('Faltan datos');  
    }
});

// busco el rol por ID
router.get ('/rol/:id', (req, res)=> {
    const { id } = req.params;
    console.log(id);
    mariaDBconnection.query ('SELECT * FROM apigaston.p_roles WHERE id = ?', [id], (err, rows, fields)=> {
        if(!err){
            res.json(rows[0]);
        } else {
            console.log(err);
        }
        
    });
});


module.exports = router; //Exporto el modulo para que sea accesible desde otros archivos

